/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.client.gui.FileSenderGUI;
import uk.ac.surrey.cs.tvs.comms.FieldResponseChecker;
import uk.ac.surrey.cs.tvs.comms.FileMessageConstructor;
import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.MBBResponse;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIResponse;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.FileBackedTimeoutManager;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Client class that represents a FileSender. This extends the Client class from VEC_Client and overrides methods that are invalid
 * for this type of client.
 * 
 * Since this is just another form of client the same processes for signing key and SSL certificate generation are used and the same
 * config files and structures are also used.
 * 
 * @author Chris Culnane
 * 
 */
public class FileSender extends Client {

  /**
   * Logger
   */
  protected static final Logger logger      = LoggerFactory.getLogger(FileSender.class);

  /**
   * String path to where we will store all submissions that are made
   */
  private static final String   MESSAGE_DIR = "./submissions/";

  /**
   * Main methods for starting FileSender. The key generation and importing of the certificates can be performed from the command
   * line, but currently the sending must be performed using the GUI. Starting the FileSender without any arguements uses the
   * hard-coded config file and starts the GUI.
   * 
   * @param args
   *          String array of arguments
   * @throws MaxTimeoutExceeded
   * @throws PeerSSLInitException
   * @throws IOException
   * @throws JSONIOException
   */
  public static void main(String[] args) throws JSONIOException, IOException, PeerSSLInitException, MaxTimeoutExceeded {

    if (args.length == 0) {
      FileSenderGUI fileSenderGui = new FileSenderGUI("./config/client.conf");
      fileSenderGui.launch();
    }
    else {
      try {
        FileSender fs = new FileSender(args[0]);
        switch (args[1]) {
          case "gen":
            fs.generateSigningKey();
            break;
          case "import":
            fs.importSSLCertificate(args[2]);
            break;
          case "status":
            System.out.println(fs.getNextTask());
            break;
          default:
            System.out.println(fs.getNextTask());
            break;
        }

      }
      catch (JSONIOException | ClientException | IOException | PeerSSLInitException | MaxTimeoutExceeded e) {
        logger.error("Exception running command", e);
      }
    }
  }

  /**
   * Constructs a FileSender using the specified configPath
   * 
   * @param configPath
   *          String path to config file
   * @throws JSONIOException
   * @throws IOException
   * @throws PeerSSLInitException
   * @throws MaxTimeoutExceeded
   */
  public FileSender(String configPath) throws JSONIOException, IOException, PeerSSLInitException, MaxTimeoutExceeded {
    super(configPath);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.client.Client#addBallotTimeout(java.lang.String)
   */
  @Override
  public void addBallotTimeout(String ballotID) {
    throw new UnsupportedOperationException("Not supported in FileSender");
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.client.Client#generateBallots()
   */
  @Override
  public void generateBallots() throws ClientException {
    throw new UnsupportedOperationException("Not supported in FileSender");
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.client.Client#generateCryptoKeyPair()
   */
  @Override
  public void generateCryptoKeyPair() throws ClientException {
    throw new UnsupportedOperationException("Not supported in FileSender");
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.client.Client#getTimeoutManager()
   */
  @Override
  public FileBackedTimeoutManager getTimeoutManager() {
    throw new UnsupportedOperationException("Not supported in FileSender");
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.client.Client#initPaths()
   */
  @Override
  protected void initPaths() {
    // We override this method and set the base path to the current directory because this is a special case. This provides a
    // warning in FindBugs because we are setting a static variable, but it is intentional and allows us to use the same underlying
    // code on both Android and Desktop
    BASE_PATH = "./";
  }

  /**
   * Sends a FileMessage to the MBB by constructing a zip file from the specified files.
   * 
   * A new randomly generated UUID will be created for the submissionID. This will also be mirrored in the submissions folder
   * locally where a copy of the submission and response will be stored.
   * 
   * @param description
   *          String description of the submission
   * @param files
   *          List of files to be included in the zip file and sent
   * @return JSONObject of response
   * @throws ClientException
   */
  public JSONObject sendFileMessage(String description, List<File> files) throws ClientException {
    try {
      // Create a new UUID that will be the submissionID
      String msgID = UUID.randomUUID().toString();

      // Create an output folder for it - check we haven't got a duplicate
      File submissionFolder = new File(FileSender.MESSAGE_DIR, msgID);
      if (submissionFolder.exists()) {
        throw new ClientException("Duplicate UUID generated");
      }
      else {
        boolean result = submissionFolder.mkdirs();
        if (result == false) {
          throw new ClientException("Failed to create submission directory");
        }
      }

      // Prepare the zipfile
      File zipOutputFile = new File(submissionFolder, msgID + ".zip");
      if (zipOutputFile.exists()) {
        throw new ClientException("Zip file already exists!");
      }
      ZipOutputStream zos = null;
      // We won't actually use this digest, but this allows us to reuse the existing code
      MessageDigest md = MessageDigest.getInstance(ClientConstants.DEFAULT_MESSAGE_DIGEST);
      try {
        zos = new ZipOutputStream(new FileOutputStream(zipOutputFile));
        for (File fileToAdd : files) {
          // Add each file to the zip
          if (fileToAdd.isFile() && fileToAdd.exists()) {
            ZipUtil.addFileToZip(fileToAdd, md, zos);
          }
          else {
            throw new ClientException("File " + fileToAdd + " is either not a file or does not exist");
          }

        }
      }
      catch (IOException e) {
        throw new ClientException("IOException whilst creating zip file", e);
      }
      finally {
        if (zos != null) {
          try {
            zos.close();
          }
          catch (IOException e) {
            throw new ClientException("Exception closing zip stream", e);
          }
        }
      }
      // Send the constructed zip file to the MBB and get the response
      JSONObject output = this.sentToMBB(zipOutputFile, msgID, description);

      // Save the response to the submission folder
      IOUtils.writeJSONToFile(output, submissionFolder.getAbsolutePath() + "/response.json");

      // Add the path to the output Object so the caller can easily find the saved response
      output.put("responseFile", submissionFolder.getAbsolutePath() + "/response.json");
      return output;

    }
    catch (NoSuchAlgorithmException | TVSSignatureException | JSONException | IOException | CryptoIOException | KeyStoreException
        | JSONIOException | MBBCommunicationException e) {
      throw new ClientException("Exception whilst creating and sending file message", e);

    }
  }

  /**
   * Does the actual sending of the messge to the MBB. This uses the same underlying MBB code from the vVoteLibrary to perform the
   * send and response processing.
   * 
   * @param zipOutputFile
   *          File to send
   * @param msgID
   *          String submissionID
   * @param description
   *          String description of submission
   * @return JSONObject containing response and, if applicable, combined signature
   * @throws KeyStoreException
   * @throws CryptoIOException
   * @throws NoSuchAlgorithmException
   * @throws TVSSignatureException
   * @throws JSONException
   * @throws IOException
   * @throws JSONIOException
   * @throws MBBCommunicationException
   */
  private JSONObject sentToMBB(File zipOutputFile, String msgID, String description) throws KeyStoreException, CryptoIOException,
      NoSuchAlgorithmException, TVSSignatureException, JSONException, IOException, JSONIOException, MBBCommunicationException {
    
    //Load and prepare keystores - we do this each time since we aren't expecting many file submissions to occur, no need to preload
    TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    tvsKeyStore.load(this.getClientConf().getStringParameter(ClientConfig.SIGNING_KEY_STORE), "".toCharArray());

    TVSKeyStore clientKeyStore = CryptoUtils.loadTVSKeyStore(this.getClientConf().getStringParameter(
        ClientConfig.SYSTEM_CERTIFICATE_STORE));

    //Construct the FileMessage using the parameters
    JSONObject msg = FileMessageConstructor.create(zipOutputFile, tvsKeyStore, ClientConfig.SIGNING_KEY_ALIAS, this.getClientConf()
        .getStringParameter(ClientConfig.ID), msgID, description);

    //Load the MBBConfig
    MBBConfig mbbConf = new MBBConfig(this.getClientConf().getStringParameter(ClientConfig.MBB_CONF_FILE));

    //Prepare a signature checker string
    StringBuffer dataToCheck = new StringBuffer();
    dataToCheck.append(msg.getString(FileMessage.ID));
    dataToCheck.append(msg.getString(FileMessage.DIGEST));
    dataToCheck.append(msg.getString(JSONWBBMessage.SENDER_ID));

    //Send and wait for a threshold response
    MBBResponse mbbResponse = MBB.sendMessageWaitForThreshold(mbbConf,
        msg.toString() + MBB.FILE_SUB_SEPARATOR + zipOutputFile.getAbsolutePath(), this.getSocketFactory(), this.getCipherSuite(),
        new FieldResponseChecker(clientKeyStore, SystemConstants.SIGNING_KEY_SK2_SUFFIX, dataToCheck.toString(), "",
            MessageFields.PeerResponse.COMMIT_TIME), this.getClientConf().getIntParameter(ClientConfig.THRESHOLD));
    logger.info("Responses received: {}", mbbResponse.getResponseArray().toString());
    JSONObject response;

    //Verify and combine
    TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, clientKeyStore);
    tvsSig.update(dataToCheck.toString());
    if (mbbResponse.getDataItem() != null) {
      tvsSig.update(mbbResponse.getDataItem());
    }
    response = tvsSig.verifyAndCombine(mbbResponse.getResponseArray(), SystemConstants.SIGNING_KEY_SK2_SUFFIX, this.getClientConf()
        .getIntParameter(ClientConfig.THRESHOLD), mbbConf.getPeers().size(), !mbbResponse.isThresholdChecked(), true);
    
    //Construct output from message, response and filename
    JSONObject output = new JSONObject();
    output.put("message", msg);
    output.put("filename", zipOutputFile.getAbsolutePath());
    output.put("response", response);
    if (response.has(TVSSignature.COMBINED)) {
      logger.info("Signature on response verified");
      output.put(UIResponse.WBB_SIGNATURES, response.getString(TVSSignature.COMBINED));
    }
    else {
      logger.info("Signature on response invalid");
    }
    return output;
  }

}
