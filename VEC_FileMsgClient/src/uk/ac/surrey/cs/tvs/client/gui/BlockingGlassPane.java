/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;

/**
 * Simple GlassPane component that will be overlaid to block UI access whilst a send is in operation. This is a quick way to disable
 * all UI actions whilst another action is being performed on a separate thread.
 * 
 * @author Chris Culnane
 * 
 */
public class BlockingGlassPane extends JComponent implements KeyListener {

  /**
   * long serialVersionUID for serialization
   */
  private static final long serialVersionUID = 9052785287279613208L;

  /**
   * Creates a new BlockingGlassPane with a Please wait message. This will appear above all other components and intercept mouse
   * interactions
   */
  public BlockingGlassPane() {
    // Set glass pane properties
    JLabel waitMessage = new JLabel("Please wait...");

    this.setOpaque(false);
    Color base = UIManager.getColor("inactiveCaptionBorder");
    Color background = new Color(base.getRed(), base.getGreen(), base.getBlue(), 128);
    this.setBackground(background);
    this.setLayout(new GridBagLayout());

    // Add a message label to the glass pane

    this.add(waitMessage, new GridBagConstraints());
    waitMessage.setOpaque(true);

    // Disable Mouse, Key and Focus events for the glass pane

    this.addMouseListener(new MouseAdapter() {
    });
    this.addMouseMotionListener(new MouseMotionAdapter() {
    });

    this.addKeyListener(this);

    this.setFocusTraversalKeysEnabled(false);
  }

  /**
   * Hide the glass pane and remove the busy cursour
   */
  public void hidePane() {
    this.setCursor(null);
    this.setVisible(false);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
   */
  @Override
  public void keyPressed(KeyEvent e) {
    e.consume();

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
   */
  @Override
  public void keyReleased(KeyEvent e) {
    e.consume();

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
   */
  @Override
  public void keyTyped(KeyEvent e) {

  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
   */
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.setColor(this.getBackground());
    g.fillRect(0, 0, this.getSize().width, this.getSize().height);
  }

  /**
   * Sets the glasspane to being visible, thus blocking interactions and shows the cursor as busy.
   */
  public void showPane() {
    this.setVisible(true);
    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    this.requestFocusInWindow();
  }
}
