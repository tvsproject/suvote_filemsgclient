/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client.gui;

import java.awt.HeadlessException;
import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.client.FileSender;

/**
 * SendFileWorker performs the actual sending on a different thread so as to not block the UI thread
 * 
 * @author Chris Culnane
 * 
 * @param <V>
 */
public class SendFilesWorker<V> extends SwingWorker<JSONObject, Void> {

  /**
   * Logger
   */
  protected static final Logger logger = LoggerFactory.getLogger(SendFilesWorker.class);

  /**
   * String containing the description of the submission
   */
  private String                description;

  /**
   * List of Files that should be sent
   */
  private List<File>            files;

  /**
   * Underlying FileSender to be used to send the actual files
   */
  private FileSender            fileSender;

  /**
   * FileSenderGUI that we call when done
   */
  private FileSenderGUI         fileSenderGUI;

  /**
   * Reference to the ProgressBar so it can be disabled on completion
   */
  private JProgressBar          progressBar;

  /**
   * Creates a new SendFileWorker with the necessary parameters and references
   * 
   * @param fileSenderGUI
   *          FileSenderGUI that is calling this worker thread
   * @param fileSender
   *          FileSender to use for sending the actual message
   * @param files
   *          List of Files that should be sent
   * @param description
   *          String description of submission
   * @param progressBar
   *          JProgressBar to hide when complete
   */
  public SendFilesWorker(FileSenderGUI fileSenderGUI, FileSender fileSender, List<File> files, String description,
      JProgressBar progressBar) {
    this.files = files;
    this.fileSender = fileSender;
    this.description = description;
    this.fileSenderGUI = fileSenderGUI;
    this.progressBar = progressBar;

  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.swing.SwingWorker#doInBackground()
   */
  @Override
  protected JSONObject doInBackground() throws Exception {
    // Send the actual files
    return this.fileSender.sendFileMessage(this.description, this.files);
  }

  /*
   * (non-Javadoc)
   * 
   * @see javax.swing.SwingWorker#done()
   */
  @Override
  protected void done() {
    try {
      // Hide the progress bar and remove the GlassPane blocking the UI
      this.progressBar.setVisible(false);
      JRootPane rootPane = SwingUtilities.getRootPane(this.fileSenderGUI.getFrame());
      rootPane.getGlassPane().setVisible(false);
      rootPane.remove(rootPane.getGlassPane());

      // Get the response and check whether it is valid
      JSONObject response = this.get();
      if (response.getJSONObject("response").getInt("validCount") >= this.fileSender.getClientConf().getIntParameter("threshold")) {
        // Show success message
        JOptionPane.showMessageDialog(this.fileSenderGUI.getFrame(),
            "Submission success!\nResponse is at: " + response.getString("responseFile"), "Submission Success",
            JOptionPane.INFORMATION_MESSAGE);
        // Reset UI lists
        this.fileSenderGUI.reset();
        logger.info("Send success");

      }
      else {
        // Show failed message
        JOptionPane.showMessageDialog(this.fileSenderGUI.getFrame(),
            "Submission failed!\nSee response at: " + response.getString("responseFile"), "Submission Failed",
            JOptionPane.ERROR_MESSAGE);
        logger.warn("Send failed: {}", response);
      }
    }
    catch (InterruptedException | ExecutionException | HeadlessException | JSONException e) {
      logger.error("Exception processing done message", e);

    }

  }

}
